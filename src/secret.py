import os

# imgur_id = os.environ['imgur_id']

reddit_client_id = os.environ['reddit_client_id']
reddit_client_secret = os.environ['reddit_client_secret']
reddit_user = os.environ['reddit_user']
reddit_password = os.environ['reddit_password']

streamable_pass = os.environ['streamable_pass']
streamable_user = os.environ['streamable_user']
